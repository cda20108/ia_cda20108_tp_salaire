package fr.afpa.beans;


public final class Representant extends Vendeur {

	public Representant(String nom, String prenom, int age, String dateEntree, int CA) {
		super(nom, prenom, age, dateEntree, CA);

	}

	public Representant() {}

	@Override
	public double calculerSalaire() {
		super.setSalaire(super.getChiffreAffaire()*0.2 + 800);
		return super.getSalaire();
	}

	@Override
	public String toString() {
		return "Le Representant " + super.getNom() + " " + super.getPrenom();
	}
	
}

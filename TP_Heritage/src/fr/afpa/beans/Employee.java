package fr.afpa.beans;


public abstract class Employee {

	private String nom;
	private String prenom;
	private int age;
	private String dateEntree;
	private double salaire;
	
	public Employee(String nom, String prenom, int age, String dateEntree) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.dateEntree = dateEntree;
	}
	
	public Employee() {}
	
	public abstract double calculerSalaire();
	
	public double getSalaire() {
		return salaire;
	}
	
	public void setSalaire(double salaire) {
		this.salaire = salaire;
	}
	
	public String getNom() {
		return nom;
	}
	
	public String getPrenom() {
		return prenom;
	}
	
	
	@Override
	public String toString() {
		return "L'employ� " + nom + " " + prenom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getDateEntree() {
		return dateEntree;
	}

	public void setDateEntree(String dateEntree) {
		this.dateEntree = dateEntree;
	}
	
	
	
}

package fr.afpa.beans;


public class Manutentionnaire extends Employee {
	
	private int heureTaff;

	public Manutentionnaire(String nom, String prenom, int age, String dateEntree, int heures) {
		super(nom, prenom, age, dateEntree);
		this.heureTaff = heures;
	}

	public Manutentionnaire() {}

	@Override
	public double calculerSalaire() {
		super.setSalaire(heureTaff*20);
		return super.getSalaire();
	}

	public int getHeureTaff() {
		return heureTaff;
	}

	public void setHeureTaff(int heureTaff) {
		this.heureTaff = heureTaff;
	}
	
	@Override
	public String toString() {
		return "Le manutentionnaire " + super.getNom() + " " + super.getPrenom();
	}

}

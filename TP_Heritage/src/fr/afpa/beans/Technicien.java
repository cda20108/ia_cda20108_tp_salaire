package fr.afpa.beans;


public class Technicien extends Employee {
	
	private int nbUnites;

	public Technicien(String nom, String prenom, int age, String dateEntree, int unite) {
		super(nom, prenom, age, dateEntree);
		this.nbUnites = unite;
	}

	public Technicien() {}

	@Override
	public double calculerSalaire() {
		super.setSalaire(nbUnites*5);
		return super.getSalaire();
	}

	public int getNbUnites() {
		return nbUnites;
	}

	public void setNbUnites(int nbUnites) {
		this.nbUnites = nbUnites;
	}
	
	@Override
	public String toString() {
		return "Le technicien " + super.getNom() + " " + super.getPrenom();
	}

}
